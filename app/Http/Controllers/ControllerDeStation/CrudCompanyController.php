<?php

namespace App\Http\Controllers\ControllerDeStation;

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CrudCompanyController extends Controller
{
    //

    public function getListCompany(){
        $listCompany = CompanyController::getAllCompany();
        return response()->json($listCompany);
    }
}
