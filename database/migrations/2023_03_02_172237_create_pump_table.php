<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pump', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->dateTime('last_update')->nullable();
            $table->string('reference')->nullable();
            $table->bigInteger('tank_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pump');
    }
};
