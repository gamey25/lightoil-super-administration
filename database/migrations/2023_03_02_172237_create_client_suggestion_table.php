<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_suggestion', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->string('attach_copy1')->nullable();
            $table->string('attach_copy2')->nullable();
            $table->string('mail_sender')->nullable();
            $table->string('message')->nullable();
            $table->string('sending_date')->nullable();
            $table->bigInteger('service_station_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_suggestion');
    }
};
