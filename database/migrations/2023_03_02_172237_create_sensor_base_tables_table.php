<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensor_base_tables', function (Blueprint $table) {
            $table->comment('');
            $table->bigIncrements('id');
            $table->string('eui');
            $table->string('reference');
            $table->timestamps();
            $table->boolean('in_maintenance')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensor_base_tables');
    }
};
