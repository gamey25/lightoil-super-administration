<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remaining_day_notification', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->dateTime('last_update')->nullable();
            $table->double('liquid_temperature');
            $table->double('percent');
            $table->bigInteger('remaining_day')->nullable();
            $table->bigInteger('tank_id');
            $table->string('type')->nullable();
            $table->double('volume');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remaining_day_notification');
    }
};
