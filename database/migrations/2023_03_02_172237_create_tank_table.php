<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tank', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->longText('abacus')->nullable();
            $table->double('diameter')->nullable();
            $table->string('file_path')->nullable();
            $table->date('last_update')->nullable();
            $table->string('liquid_type')->nullable();
            $table->double('man_hole_height')->nullable();
            $table->double('sensor_depth')->nullable();
            $table->string('sensor_reference')->nullable();
            $table->bigInteger('station_product_id')->nullable();
            $table->bigInteger('time_out')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tank');
    }
};
