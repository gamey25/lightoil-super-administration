<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quart_working', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->double('quart_number');
            $table->string('quarts')->nullable();
            $table->bigInteger('service_station_id');
            $table->binary('time_close')->nullable();
            $table->binary('time_start')->nullable();
            $table->bigInteger('time_start_day')->nullable();
            $table->bigInteger('time_end_day')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quart_working');
    }
};
