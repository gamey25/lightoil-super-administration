<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->double('battery')->nullable();
            $table->dateTime('last_update')->nullable();
            $table->double('liquid_height')->nullable();
            $table->double('liquid_temperature')->nullable();
            $table->double('percent')->nullable();
            $table->bigInteger('remaining_day')->nullable();
            $table->bigInteger('tank_id')->nullable();
            $table->string('type')->nullable();
            $table->double('volume')->nullable();
            $table->string('list_view_user_id')->nullable();
            $table->string('list_user_view_notification')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
};
