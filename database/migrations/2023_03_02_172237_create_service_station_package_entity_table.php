<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_station_package_entity', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->bigInteger('depotage_notification');
            $table->bigInteger('history_notification');
            $table->bigInteger('investigations');
            $table->bigInteger('kit');
            $table->string('last_update')->nullable();
            $table->bigInteger('package_configuration');
            $table->bigInteger('quart_configuration');
            $table->bigInteger('remaining_day_notification');
            $table->bigInteger('reports');
            $table->bigInteger('scdpdelay_configuration');
            $table->bigInteger('service_station_id')->nullable();
            $table->bigInteger('volumes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_station_package_entity');
    }
};
