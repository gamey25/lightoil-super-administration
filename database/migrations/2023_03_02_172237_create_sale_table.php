<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->string('description')->nullable();
            $table->string('full_name')->nullable();
            $table->dateTime('last_update')->nullable();
            $table->bigInteger('pump_id')->nullable();
            $table->double('real_price')->nullable();
            $table->double('real_volume')->nullable();
            $table->double('theorical_price')->nullable();
            $table->double('theorical_volume')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale');
    }
};
