<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role_service_station', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->bigInteger('service_station_id')->nullable();
            $table->bigInteger('user_role_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role_service_station');
    }
};
