<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correction_datas', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->string('correction_name')->nullable();
            $table->dateTime('created_at');
            $table->longText('data_attribute')->nullable();
            $table->string('data_attributejson')->nullable();
            $table->double('density')->nullable();
            $table->string('liquid_type')->nullable();
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correction_datas');
    }
};
