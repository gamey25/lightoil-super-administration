<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depotage', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->dateTime('end_at')->nullable();
            $table->double('end_height')->nullable();
            $table->double('end_liquid_temperature')->nullable();
            $table->double('end_volume')->nullable();
            $table->double('start_height')->nullable();
            $table->double('start_liquid_temperature')->nullable();
            $table->double('start_volume')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('taked_at')->nullable();
            $table->bigInteger('tank_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depotage');
    }
};
