<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmfd_commands_entity', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->bigInteger('driver_name_id');
            $table->bigInteger('gespar_code_id');
            $table->dateTime('last_update')->nullable();
            $table->bigInteger('operations_manager_id');
            $table->bigInteger('quantity_requested');
            $table->bigInteger('requesting_agency_id');
            $table->bigInteger('stocks_manager_id');
            $table->bigInteger('tmfd_action_id');
            $table->bigInteger('type_material_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmfd_commands_entity');
    }
};
