<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_materials_entity', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->bigInteger('company_id');
            $table->dateTime('last_update')->nullable();
            $table->string('name_type_material')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_materials_entity');
    }
};
