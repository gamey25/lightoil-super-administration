<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remaining_notification_parameters', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->string('last_update')->nullable();
            $table->bigInteger('scdp_delay_day');
            $table->bigInteger('service_station_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remaining_notification_parameters');
    }
};
