<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->string('address')->nullable();
            $table->binary('bd_image')->nullable();
            $table->string('custimage')->nullable();
            $table->string('email')->nullable()->unique('UK_ob8kqyqqgmefl0aco34akdtpe');
            $table->binary('f')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('last_update')->nullable();
            $table->string('password')->nullable();
            $table->string('path_custimage')->nullable();
            $table->string('phone')->nullable()->unique('UK_589idila9li6a4arw1t8ht1gx');
            $table->string('status')->nullable();
            $table->string('token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
};
