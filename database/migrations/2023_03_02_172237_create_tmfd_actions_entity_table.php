<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmfd_actions_entity', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->bigInteger('company_id');
            $table->dateTime('last_update')->nullable();
            $table->string('tmfd_action')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmfd_actions_entity');
    }
};
