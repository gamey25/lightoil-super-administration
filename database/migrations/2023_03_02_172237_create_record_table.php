<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->double('battery_level')->nullable();
            $table->double('density')->nullable();
            $table->integer('depotage');
            $table->double('env_temperature')->nullable();
            $table->dateTime('last_update')->nullable();
            $table->double('level')->nullable();
            $table->double('liquid_height')->nullable();
            $table->double('liquid_temperature')->nullable();
            $table->string('sensor_reference')->nullable();
            $table->bigInteger('tank_id')->nullable();
            $table->double('total_volume')->nullable();
            $table->double('volume')->nullable();
            $table->double('volume_at_fift')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record');
    }
};
