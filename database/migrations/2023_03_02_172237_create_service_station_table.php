<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_station', function (Blueprint $table) {
            $table->comment('');
            $table->bigInteger('id')->primary();
            $table->string('city')->nullable();
            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('gmt')->nullable();
            $table->string('last_update')->default(Carbon::now());
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('name')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_station');
    }
};
